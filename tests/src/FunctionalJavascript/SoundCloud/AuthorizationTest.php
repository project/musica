<?php

declare(strict_types=1);

namespace Drupal\Tests\musica\FunctionalJavascript\SoundCloud;

use Drupal\Core\Config\Config;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use PHPUnit\Framework\Attributes\CoversNothing;
use PHPUnit\Framework\Attributes\Group;

/**
 * Live tests for authorization flow.
 *
 * @see https://www.drupal.org/docs/develop/automated-testing/phpunit-in-drupal/running-phpunit-javascript-tests
 * @see https://www.drupal.org/docs/automated-testing/phpunit-in-drupal/phpunit-javascript-test-writing-tutorial
 *
 * @group ignore
 */
#[CoversNothing]
#[Group('javascript')]
class AuthorizationTest extends WebDriverTestBase {

  /**
   * The modules to load to run the test.
   *
   * @var array<string>
   */
  protected static $modules = ['musica'];

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Test that the module is able to retrieve the authoriztion page.
   *
   * That's as far as we can live-test since the rest of the authentication flow
   * involves entering user credentials and sovling a human-solvable captcha.
   */
  public function testGetAuthorizationPage(): void {
    $this->configFromEnv();

    /** @var \Drupal\musica\Service\SoundCloud\Auth $service */
    $service = $this->container->get('musica.soundcloud.auth');
    $service->diagnose(TRUE);
    $service->init();

    $authUrl = $service->provider->getAuthorizationUrl();
    $oauth2state = $service->provider->getState();

    // @todo Perms check.
    // What's the current access permisions for this page? And why?
    $this->drupalLogin($this->rootUser);

    $assert = $this->assertSession();
    $mink = $this->getSession();
    $mink->setCookie('oauth2state', $oauth2state);
    $mink->visit($authUrl);

    // Cannot assert HTTP status, but auth URL can be checked.
    // @see https://drupal.stackexchange.com/a/291178/693
    $assert->addressMatches('/connect/');

    // There's a small delay before Javascript finishes rendering the page.
    $mink->wait(3000);

    // Webdriver is visitng as a logged out user.
    $assert->elementExists('css', 'body #app form.connect-form.sign-in-up-form');
  }

  /**
   * Set up config from env.
   */
  public function configFromEnv(): Config {
    $config = $this->config('musica.settings');

    if ($soundcloud_client_id = getenv('soundcloud_client_id')) {
      $config->set('soundcloud_client_id', $soundcloud_client_id);
    }
    if ($soundcloud_client_secret = getenv('soundcloud_client_secret')) {
      $config->set('soundcloud_client_secret', $soundcloud_client_secret);
    }
    $config->save();
    return $config;
  }

}
