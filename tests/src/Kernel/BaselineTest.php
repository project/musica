<?php

declare(strict_types=1);

namespace Drupal\Tests\Musica\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\musica\CoverageClass;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;

/**
 * Dummy case for testing PHPUnit coverage and reporting settings.
 */
#[CoversClass(CoverageClass::class)]
#[Group('ignore')]
class BaselineTest extends KernelTestBase {

  /**
   * Test dummy.
   */
  public function testTestPhpUnitFunctionalCoverage(): void {
    $class = new CoverageClass();
    self::assertEquals('hello world', $class->hello(), 'hello world test');

    $this->assertSame(TRUE, TRUE);
  }

}
