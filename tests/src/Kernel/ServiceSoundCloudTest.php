<?php

declare(strict_types=1);

namespace Drupal\Tests\musica\Kernel;

use DateInterval;
use DateTimeImmutable;
use Drupal\KernelTests\KernelTestBase;
use Drupal\musica\Service\SoundCloud\Auth;
use Drupal\musica\Service\SoundCloud\AuthToken;
use Drupal\musica\Service\SoundCloud\Resource;
use League\OAuth2\Client\Token\AccessTokenInterface;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;
use Widmogrod\Monad\Either as e;
use Widmogrod\Monad\Either\Right;
use Widmogrod\Monad\Maybe as m;

// @phpcs:disable
/**
 * Dummy case for testing PHPUnit coverage and reporting settings.
 */
#[CoversClass(Auth::class)]
#[Group('target4')]
class ServiceSoundCloudTest extends KernelTestBase {

  /**
   * The modules to load to run the test.
   *
   * @var array<string>
   */
  protected static $modules = ['musica'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['musica']);
  }

  /**
   * Test the client credentials authorization flow.
   *
   * PLEASE NOTE:
   *
   * Be careful about running this test repeatedly.
   * There is a rate limit of 50 tokens / 12h, or 30 tokens / 1h / IPaddr.
   *
   * @see https://developers.soundcloud.com/docs#authentication
   */
  public function testClientCredentialsFlow(): void {
    $service = (fn (): Auth => $this->container->get('musica.soundcloud.auth'))();

    // PKCE presence is enforced even in tests!
    $this->assertInstanceOf(e\Right::class, $service->requestAuthCode(), 'Provider state and PKCE are available');

    $response = $service->getAccessToken('client_credentials', []);
    $this->assertInstanceOf(e\Right::class, $response, 'Service returns the Right response');

    $token = (fn (): AccessTokenInterface|FALSE => e\fromRight(FALSE, $response))();
    $this->assertNotFalse($token, 'Token object conforms to correct interface');
    $this->assertNotEmpty($token->getToken(), 'Access token is granted');
    $this->assertNotEmpty($token->getRefreshToken(), 'Refresh token is granted');
    $this->assertIsNumeric($token->getExpires(), 'Token comes with expiration date');
  }

  /**
   * Test the token refresh authorization flow.
   */
  public function testTokenRefreshFlow(): void {
    $service = (fn (): Auth => $this->container->get('musica.soundcloud.auth'))();

    $assertCase = fn ($case) => fn ($expected) => fn (string $message = '') =>
      $this->assertInstanceOf($expected, $service->cacheGet($case->value), $message);

    $bothTokensAreOk = fn ($case) =>
      $assertCase($case)
      (m\Just::class)
      ('Both tokens are fresh');

    // Get and store a fresh token using client credentials flow for testing.
    $token = (fn (): AccessTokenInterface|FALSE =>
      e\fromRight(FALSE, $service->getAccessToken('client_credentials', []))
    )();
    $service->cacheToken($token);
    array_map($bothTokensAreOk, AuthToken::cases());

    // Invalidate ONLY the access token, leaving the refresh token intact.
    // These conditionas simulate when access tokens becoming expired.
    $service->revoke(AuthToken::ACCESS);
    $assertCase(AuthToken::ACCESS)(m\Nothing::class)('Access token is now invalid');
    $assertCase(AuthToken::REFRESH)(m\Just::class)('Refresh token should still be valid');

    // If you want to inspect the tokens yourself, you can run this.
    // $_access = m\fromMaybe(FALSE, $service->cacheGet(AuthToken::ACCESS->value));
    // $_refresh = m\fromMaybe(FALSE, $service->cacheGet(AuthToken::REFRESH->value));

    // Call to authorize should trigger the refresh auhorization flow.
    $this->assertTrue(e\fromRight(FALSE, $service->authorize()), 'Service completes authorization');
    array_map($bothTokensAreOk, AuthToken::cases());

    $previous = $token->getRefreshToken();
    $this->assertNotEquals(
      $previous, m\fromMaybe($previous, $service->cacheGet(AuthToken::REFRESH->value)),
      'The new refresh token differs from the previous refresh token'
    );
  }

  public function testFetchResource(): void {
    $service = (fn (): Auth => $this->container->get('musica.soundcloud.auth'))();
    $service->requestAuthCode();
    $token = e\fromRight(FALSE, $service->getAccessToken('client_credentials'));
    $service->cacheToken($token);

    $resource = new Resource($service);
    $response = $resource->getResource('tracks/308946187');
    $this->assertInstanceOf(Right::class, $response, 'Service is able to retrieve resource');
    $object = $resource->getResourceObject($response);
    $this->assertInstanceOf(Right::class, $object);
  }

}
