<?php

declare(strict_types=1);

namespace Drupal\Tests\musica\Unit;

use Drupal\musica\CoverageClass;
use Drupal\Tests\UnitTestCase;

/**
 * Dummy case for testing PHPUnit coverage and reporting settings.
 *
 * @group phpunit-9.5
 * @group musica
 * @covers Drupal\musica\CoverageClass
 */
class BaselineTest extends UnitTestCase {

  /**
   * Test dummy.
   */
  public function testTestPhpUnitFunctionalCoverage(): void {
    $class = new CoverageClass();
    self::assertEquals('hello world', $class->hello(), 'hello world test');
  }

}
