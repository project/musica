<?php

declare(strict_types=1);

// phpcs:disable

namespace Drupal\Tests\musica\Unit\SoundCloud;

use CuyZ\Valinor\Mapper\MappingError;
use CuyZ\Valinor\Mapper\Source\Exception\InvalidSource;
use DateInterval;
use DateTimeImmutable;
use Drupal\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\Context\UserCacheContext;
use Drupal\Core\Cache\MemoryBackend;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Messenger\Messenger;
use Drupal\musica\API\SoundCloud\Track;
use Drupal\musica\API\SoundCloud\User;
use Drupal\musica\Service\SoundCloud\Auth;
use Drupal\musica\Service\SoundCloud\AuthException;
use Drupal\musica\Service\SoundCloud\AuthStatus;
use Drupal\musica\Service\SoundCloud\AuthToken;
use Drupal\musica\Service\SoundCloud\Client;
use Drupal\musica\Service\SoundCloud\Provider;
use Drupal\musica\Service\SoundCloud\Resource;
use Drupal\Tests\UnitTestCase;
use League\OAuth2\Client\Token\AccessTokenInterface;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\DependsOnClassUsingDeepClone;
use PHPUnit\Framework\Attributes\DependsUsingDeepClone;
use PHPUnit\Framework\Attributes\DoesNotPerformAssertions;
use PHPUnit\Framework\Attributes\Group;
use PHPUnit\Framework\Attributes\RequiresPhpunit;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Exception;
use PHPUnit\Framework\ExpectationFailedException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\SkippedWithMessageException;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use PHPUnit\Framework\UnknownClassOrInterfaceException;
use Symfony\Component\HttpFoundation\Session\Session;
use Widmogrod\Monad\Either as e;
use Widmogrod\Monad\Maybe as m;

// phpcs:enable
// phpcs:disable Drupal.Classes.FullyQualifiedNamespace.UseStatementMissing

/**
 * Provides testing for mocked API resources.
 *
 * Mocking string translation trait.
 *
 * @see https://drupal.stackexchange.com/a/233286/693
 */
#[RequiresPhpunit('>= 10.5.0')]
#[CoversClass(Auth::class)]
#[CoversClass(Resource::class)]
#[Group('group10')]
class ServiceTest extends UnitTestCase {

  /**
   * Mocked service container.
   */
  public ContainerInterface $container;

  /**
   * Mocked SoundCloud service.
   */
  public MockObject&Auth $service;

  /**
   * Namespace for value objects (DTOs) under test.
   */
  public string $namespace = 'Drupal\musica\API\SoundCloud';

  /**
   * Container for hydrated value objects.
   *
   * @var array<mixed>
   */
  public static array $mapped;

  /**
   * What it says on the tin.
   */
  public function fakeToken(): MockObject&AccessTokenInterface {
    // Mock token with one hour expiration date.
    $fakeToken = fn (?string $refresh = NULL) => fn (?string $token = NULL) => fn (?int $expire = NULL): array => [
      'getExpires' => $expire ??= (new DateTimeImmutable())->add(new DateInterval('PT1H'))->getTimestamp(),
      'getRefreshToken' => $refresh ??= $this->randomString(33),
      'getToken' => $token ??= $this->randomString(33),
    ];
    $valid = $fakeToken("2mwgV5igzzV441Rl82PV0ZkjEJlAElqa")(NULL)(NULL);
    return $this->createConfiguredMock(AccessTokenInterface::class, $valid);
  }

  /**
   * Sets up the service mock and dependencies.
   */
  public function setUp(): void {
    parent::setUp();

    $this->service = $this->getMockBuilder(Auth::class)
      ->onlyMethods(['cacheGet'])
      ->setConstructorArgs([
        $this->createStub(Client::class),
        $this->createStub(UserCacheContext::class),
        $this->createStub(MemoryBackend::class),
        $this->createStub(Session::class),
      ])
      ->getMock();

    $this->container = new ContainerBuilder();
    $this->container->set('string_translation', $this->getStringTranslationStub());
    \Drupal::setContainer($this->container);
  }

  /**
   * Map requested resource to value object.
   *
   * @param \Drupal\musica\Service\SoundCloud\Auth $service
   *   Service (mock) to inject into a real resource instance.
   * @param string $resource
   *   Resource id to grab from mock API.
   * @param class-string<T> $mapTo
   *   Value object used for hydrating or mapping resource.
   *
   * @return T
   *   Mapped value object.
   *
   * @template T
   */
  public function mapResource(Auth $service, string $resource, string $mapTo): mixed {
    $r = new Resource($service);

    $raw = $r->getResource($resource);
    $this->assertIsString(e\fromRight(NULL, $raw), 'getResource returns raw data wrapped by Either monad');

    $mappedEither = $r->getResourceObject($raw, $mapTo);
    $this->assertInstanceOf(e\Right::class, $mappedEither, 'Resource is mapped to value object');

    $mappedValue = e\fromRight(NULL, $mappedEither);
    $this->assertInstanceOf($mapTo, $mappedValue, 'Resource is mapped to correct value object');

    return $mappedValue;
  }

  /**
   * Test suite data provider.
   *
   * @return array<mixed>
   *   Fake test data.
   */
  public static function schemaProvider(): array {
    $data = [];
    $rn = fn ($name) => strtolower($name) . '/' . rand(100000000, 900000000);
    array_map(function (Schema $case) use ($rn, &$data) {
      $data[$rn($case->name)] = [
        $case->name,
        $rn($case->name),
        $case->value,
      ];
    }, Schema::cases());
    return $data;
  }

  /**
   * Warms the service cache then hydrates value objects with the fake data.
   *
   * This test operation:
   * - Modifies the service cacheGet method to return the consumed data set.
   * - Supports additional cacheGet operations such as retrieving tokens.
   * - Provides partial coverage for the Resource class via `mapResource()`.
   * - Performs generic assertions about mapping fake data to value objects via
   *   `mapResource()`.
   * - Makes the result of the mapping operations available to other tests via
   *   the static `$mapped` class property.
   *
   * @param class-string<T> $class
   *   Value object used for hydrating or mapping resource.
   * @param string $resource
   *   Fake resource name.
   * @param string $content
   *   Resource contents.
   *
   * @template T
   */
  #[Test]
  #[DataProvider('schemaProvider')]
  #[TestDox('Map $_dataName data set to $class schema')]
  public function hydration(string $class, string $resource, string $content): void {
    static $array = [];
    $cacheGet = fn ($cid) => match ($cid) {
      AuthToken::ACCESS->value => m\just($this->fakeToken()),
      AuthToken::REFRESH->value => m\just($this->fakeToken()),
      $resource => m\just($content),
      // Use Nothing if you want to probe deeper into getResource().
      // Will trigger calls to oauth client, guzzle, and cache get/set.
      // 'tracks/308946187' => m\nothing(),
      default => m\nothing(),
    };

    /** @var class-string $mapTo */
    $mapTo = "{$this->namespace}\\$class";

    $this->service->method('cacheGet')->willReturnCallback($cacheGet);
    self::$mapped[$mapTo] = $this->mapResource($this->service, $resource, $mapTo);
  }

  /**
   * Returns the hydrated value object if found, or marks the test for skipping.
   *
   * @param class-string<T> $class
   *   Target class name.
   *
   * @return T
   *   Hydrated value object.
   *
   * @template T
   */
  public function hydrated(string $class): mixed {
    if (array_search($class, array_keys(self::$mapped)) === FALSE) {
      $this->markTestSkipped("Value object for class $class not found.");
    }
    return self::$mapped[$class];
  }

  /**
   * Detailed test for Track schema.
   */
  #[Depends('hydration')]
  public function testTrackProperties(): void {
    $dto = $this->hydrated(Track::class);

    $this->assertEquals(1234, $dto->id);
    $this->assertEquals("Some title", $dto->title);
    $this->assertEquals('Rock', $dto->genre);
    $this->assertIsBool($dto->streamable);

    $this->assertEquals('https://api.soundcloud.com/tracks/1234', $dto->uri);
    $this->assertEquals('https://i1.sndcdn.com/artworks-large.jpg', $dto->artwork_url);
    $this->assertEquals('https://api.soundcloud.com/tracks/1234/download', $dto->download_url);
    $this->assertEquals('https://soundcloud.com/userPermalink/trackPermalink', $dto->permalink_url);
    $this->assertEquals('https://api.soundcloud.com/tracks/1234/stream', $dto->stream_url);
    $this->assertEquals('https://wave.sndcdn.com/someString.png', $dto->waveform_url);

    $this->assertInstanceOf(\Drupal\musica\API\SoundCloud\User::class, $dto->user);
    $this->assertEquals(\Drupal\musica\API\SoundCloud\Access::Playable, $dto->access);
  }

  /**
   * Detailed test for User schema.
   */
  #[Depends('hydration')]
  public function testUserProperties(): void {
    $dto = $this->hydrated(User::class);

    $this->assertEquals(12345, $dto->id);
    $this->assertEquals('some.user', $dto->username);
    $this->assertEquals("First_name", $dto->first_name);
    $this->assertEquals('Full Name', $dto->full_name);
    $this->assertIsNumeric($dto->playlist_count);
    $this->assertEquals('https://i1.sndcdn.com/avatars.jpg', $dto->avatar_url);
    $this->assertEquals('https://soundcloud.com/permalink', $dto->permalink_url);
  }

}
