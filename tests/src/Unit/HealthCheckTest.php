<?php

declare(strict_types = 1);

namespace Drupal\Tests\musica\Unit;

use Drupal\musica\CoverageClass;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Group;

/**
 * Dummy case for testing PHPUnit coverage and reporting settings.
 */
#[CoversClass(CoverageClass::class)]
#[Group('phpunit10')]
class HealthCheckTest extends TestCase {

  public function testTestPhpUnitCoverage() {
    $class = new CoverageClass();
    $this->assertEquals('hello world', $class->hello(), 'hello world test');
  }

}
