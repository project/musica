<?php

declare(strict_types=1);

// phpcs:disable

namespace Drupal\Tests\musica\Unit\FuncionalDemo;

use Drupal\musica\Util\Just;
use Drupal\musica\Util\Maybe;
use Drupal\Tests\UnitTestCase;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\CoversFunction;
use PHPUnit\Framework\Attributes\UsesFunction;
use PHPUnit\Framework\Attributes\Group;

use function Drupal\musica\Util\lift;

/**
 * Test suite for functional utilities, types, functions.
 */
#[CoversClass(Maybe::class)]
#[Group('target2')]
class MaybeTest extends UnitTestCase {

  /**
   * PHPUnit test.
   */
  public function testMaybeType(): void {

    $hello = Maybe::just('hello');
    $nothing = Maybe::nothing();

    $this->assertTrue('hello' === $hello->getOrElse('nothing to see'));
    $this->assertTrue($hello->isJust());
    $this->assertFalse($hello->isNothing());

    $this->assertTrue('nothing to see' === $nothing->getOrElse('nothing to see'));
    $this->assertFalse($nothing->isJust());
    $this->assertTrue($nothing->isNothing());

  }

  /**
   * PHPUnit test.
   */
  public function testLiftingFunctions(): void {

    // Add represents any regular function, closure, or method.
    $add = fn (int $a, int $b): int => $a + $b;

    // Lift wraps the target in another closure.
    $wrapped = lift($add);

    // The closure accepts the functional types, executes the lifted callable,
    // wraps the result in a functional type, then returns the wrapped result.
    $result = $wrapped(Maybe::just(1), Maybe::just(5));
    $this->assertInstanceOf(Just::class, $result, 'The result is still wrapped.');

    $this->assertEquals(6, $result->getOrElse('nothing'), 'The result is unwrapped');
  }

  /**
   * Functor laws, first try.
   */
  public function testFunctorOne(): void {

    $id = fn ($value) => $value;
    $data = [1, 2, 3, 4];
    $this->assertTrue(array_map($id, $data) === $id($data));

    $add2 = fn ($a) => $a + 2;
    $times10 = fn ($a) => $a * 10;

    $composed = fn ($a) => $add2($times10($a));
    $this->assertTrue(array_map($add2, array_map($times10, $data)) === array_map($composed, $data));

    $just = Maybe::just(10);
    $nothing = Maybe::nothing();

    $this->assertTrue($just->map($id) == $id($just));
    $this->assertTrue($nothing->map($id) === $id($nothing));


    $this->assertTrue($just->map($times10)->map($add2) == $just->map($composed));
    $this->assertTrue($nothing->map($times10)->map($add2) === $nothing->map($composed));

  }

}



interface Functor {
  public function map(callable $f): Functor;
}

/**
 * ID functor.
 *
 * @template T
 *
 * https://phpstan.org/blog/generics-by-examples
 * https://phpstan.org/blog/whats-up-with-template-covariant
 * https://phpstan.org/blog/generics-in-php-using-phpdocs
 * https://www.php.net/manual/en/language.oop5.variance.php
 */
class IdentityFunctor implements Functor {

  /**
   * @param T $value
   */
  public function __construct(private mixed $value) {}

  public function map(callable $f): Functor {
    return new self($f($this->value));
  }

  /**
   * @return T
   */
  public function get(): mixed {
    return $this->value;
  }
}

/**
 * Applicative functor.
 *
 * Applicative is written as abstract and not interface so that pure and
 * apply can be implemented in map.
 */
abstract class Applicative implements Functor {
  /**
   * Creates a new applicative from any callable.
   *
   * Anything stored inside an Applicative is considered pure because there is
   * no way to modify it directly, as inspired by Haskell.
   */
  public abstract static function pure(mixed $value): Applicative;
  /**
   * Applies the stored function to the given parameter.
   */
  public abstract function apply(Applicative $f): Applicative;
  public abstract function get(): mixed;
  public function map(callable $f): Functor {
    return $this->pure($f)->apply($this);
  }
}

class IdentityApplicative extends Applicative {
  protected function __construct(private mixed $value) {}
  public static function pure(mixed $value): Applicative {
    return new static($value);
  }
  public function apply(Applicative $f): Applicative {
    return static::pure($this->get()($f->get()));
  }
  public function get(): mixed {
    return $this->value;
  }
}
