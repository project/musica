# SoundCloud Open API Specification

There is on publicly available, official OAS document for SoundCloud.

There is a Github issue requesting an OAS source, but without resolution.
See https://github.com/soundcloud/api for more information.

To create types from an OAS document you will have to generate and supply your
own OAS document, until SoundCloud offially provides one.
