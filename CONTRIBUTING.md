# CONTRIBUTING


## Installing and updating the DEV version of Musica

Use Composer to add the Musica module to your Drupal project:

```
composer require --prefer-source 'drupal/musica:1.1.x-dev@dev'

# Tell Composer to install Musica's dependencies:
composer update drupal/musica
```

While waiting for a stable version of the module is released, you can update
the Musica module's code by navigating to
`[YOUR-PROJECT]/web/modules/contrib/musica` and running `git pull origin`.

## Set up GPG in order to contribute to the module

Make sure to navigate to the Musica module and set up GPG correctly, otherwise
`git push` commands will be rejected by Gitlab:

    ❯ cd docroot/web/modules/contrib/musica
    ❯ git config user.name "Richard Allen"
    ❯ git config user.signingkey 222453D7861991F6
    ❯ git config user.email "alexanderallen@351784.no-reply.drupal.org"
