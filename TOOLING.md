# Tooling

## PHPCS

To install PHPCS:

    cd tools/phpcs
    composer install

    vendor/bin/phpcs -i

    The installed coding standards are MySource, PEAR, PSR1, PSR2, PSR12, Squiz, Zend, Drupal, DrupalPractice, VariableAnalysis and SlevomatCodingStandard

The composer manifest for phpcs includes the Drupal coder module, which you
can find out more about [here](https://www.drupal.org/docs/contributed-modules/code-review-module/installing-coder).

The project's `.vscode/settings.json` includes settings for the [PHPCS Visual Studio Code extension by Kappas](https://marketplace.visualstudio.com/items?itemName=ikappas.phpcs), which is deprecated.

For basic PHPCS usage, see [PHP_CodeSniffer/wiki/Usage](https://github.com/squizlabs/PHP_CodeSniffer/wiki/Usage).

The `phpcs.xml` located in the same directory guides the phpcs configuration.

Things this module configures differently on Gitlab

- custom_phpcs overrides default phpcs job.
- custom job fixes command running out of memory in overriden script section.
- Points Gitlab to install and use PHPCS from the `tools/phpcs` directory.
- Removes dependencies from module's `composer.json`.

### PHPCS running out of memory on Gitlab

Noted: 5/8/2024

The default PHPCS job will run as such:

    vendor/bin/phpcs -s <file> <EXTRA_ARGS>

Passing the `<file>` argument to the command will cause the `file` parameter in
`phpcs.xml` to be ignored. This will then cause phpcs to scan all directories
in the project, including vendor. This will then cause phpcs to hang, exhaust it's
available memory, and fail.

The custom job in `.musica.gitlab-ci.yml` removes `<file>` from the command and
relies on the `phpcs.xml` configuration to solve this issue.

## PHPStan

Notes about local environment

- `.vscode/settings` allows the PHPStan Visual Studio Code extension to run properly.
- `"phpstan.rootDir": "tools/phpstan",` setting allows local PHPStan to find the Composer
- There is a dedicated `phpstan.local.neon` that maps the local paths.
autoloader present in the `tools/phpstan` directory.
- See the

Autoloading in PHPStan

- [user guide: autoloading](https://phpstan.org/user-guide/autoloading)
- [autoloader command option](https://phpstan.org/user-guide/command-line-usage#--autoload-file%7C-a)
- [discovering symbols](https://phpstan.org/user-guide/discovering-symbols)
