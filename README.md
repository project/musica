--
## Install and configure Wikimedia merge plugin

Install the `wikimedia/composer-merge-plugin` Composer package in your project's
root with `composer require wikimedia/composer-merge-plugin`.

Once that's done configure your project's main `composer.json` to find and use
Musica's list of Composer dependencies:

```
    "extra": {
        "merge-plugin": {
            "include": [
                "web/modules/contrib/musica/composer.local.json"
            ],
            "recurse": true,
            "replace": false,
            "ignore-duplicates": false,
            "merge-dev": true,
            "merge-extra": false,
            "merge-extra-deep": false,
            "merge-replace": true,
            "merge-scripts": false
        },
    }
```

