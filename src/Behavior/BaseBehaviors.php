<?php

declare(strict_types = 1);

namespace Drupal\musica\Behavior;

use Drupal\musica\Behavior\BehaviorInterface;
use Drupal\musica\Service\ServiceInterface;
use Drupal\musica\State\EntityState;

/**
 * Class stub.
 */
abstract class BaseBehaviors implements BehaviorInterface {

  /**
   * A list of `callable` behaviors for this entity.
   *
   * @var list<callable>
   */
  public static array $behaviors;

  /**
   * Reference of DTO Map associated with this behavior instance.
   *
   * @var class-string<\BackedEnum>
   */
  public static $dtoMapRef;

  /**
   * Because Enums cannot be accessed as associative arrays.
   *
   * @var array
   */
  public static array $dtoArray;

  /**
   * Stateless behavior constructor.
   *
   * DO NOT pass any kind of state (data) to behavior constructors, otherwise
   * the behavioral entities get tied (coupled) to the stateful entities.
   *
   * @param non-empty-string $namespace
   *   The kind of behavioral object, used for API calls.
   * @param class-string<\BackedEnum> $behaviorDTOMap
   *   Name of `Enum` listing API methods available as keys matching Data
   *   Transfer Objects as `Enum` values.
   */
  public function __construct(
    public readonly string $namespace,
    string $behaviorDTOMap,
  ) {
    self::$dtoMapRef = $behaviorDTOMap;
    $this->assignBehaviors($behaviorDTOMap::cases());
  }

  /**
   * {@inheritdoc}
   */
  public static function behaviors() {
  }

  /**
   * {@inheritdoc}
   */
  public function getBehavior(string $b): callable {
    $prop_exists = array_key_exists($b, self::$behaviors);
    if ($prop_exists && is_callable(self::$behaviors[$b])) {
      return self::$behaviors[$b];
    }
    else {
      return $this->dummyBehavior();
    }
  }

  /**
   * Populates the class behaviors property with behavioral closures.
   */
  protected function assignBehaviors(array $behaviors): void {
    array_walk(
      $behaviors,
      fn ($behavior) => self::$behaviors[$behavior->name] = $this->createBehaviorHof($behavior->name)
    );

    // Convert Enum to simple, actually usable PHP array.
    array_walk(
      $behaviors,
      fn ($behavior) => self::$dtoArray[$behavior->name] = $behavior->value
    );
  }

  abstract public static function hydrateState(EntityState $state, string $dataKey): EntityState;

  /**
   * Dummy behavior that goes nowhere and does mostly nothing.
   *
   * Use for testing and to preserve the functional purity of the state.
   * In case a non-existant behavior is requested, the dummy callable can be
   * subbed in without state violations or side effects.
   */
  protected function dummyBehavior(EntityState $state = NULL, ServiceInterface $service = NULL, array $params = []): callable {
    return fn (EntityState $state, ServiceInterface $service = NULL, array $params = []) => (
      EntityState::create($state->name, $state, [])
    );
  }

  /**
   * Higher Order Function that returns behavioral closures.
   *
   * @TODO: 1/17 HORRIBLE documentation and no unit test, fix please :)
   */
  protected function createBehaviorHof(string $behavior_name): callable {
    return fn (EntityState $state, ServiceInterface $service, array $params = []) => (
      EntityState::create($state->name, $state, [
        $behavior_name => $service->request(
          $this->namespace,
          $behavior_name,
          [$this->namespace => $state->name, ...$params]
        ),
      ])
    );
  }

}
