<?php

declare(strict_types = 1);

// phpcs:disable Drupal.Commenting.DataTypeNamespace.DataTypeNamespace

namespace Drupal\musica\Behavior;

use CuyZ\Valinor\Mapper\Source\Source;
use CuyZ\Valinor\MapperBuilder;
use Drupal\musica\State\EntityState;
use Drupal\musica\API\LastFM\Utility\Attribute;
use Drupal\musica\API\LastFM\Utility\Search;

/**
 * Behavioral class for Artist entity.
 */
class ArtistBehaviors extends BaseBehaviors {

  public function __construct() {
    // @todo The DTO map should be a constructor argument.
    parent::__construct('artist', ArtistDTOMap::class);
  }

  /**
   * Hyrdrates and returns a new EntityState.
   */
  public static function hydrateState(EntityState $state, string $dataKey): EntityState {
    $sauce = Source::json($state->data[$dataKey]);
    /** @var array */
    $dto = (new MapperBuilder())
      ->allowSuperfluousKeys()
      ->allowPermissiveTypes()
      ->enableFlexibleCasting()
      ->mapper()
      ->map(self::$dtoArray[$dataKey], $sauce);

    return $state::mergeStateSilo('dto', $state, [$dataKey => $dto]);
  }

}

/**
 * Maps API methods to their corresponding DTO classes.
 *
 * The mapping is used for hydrating incoming Artist data.
 *
 * @todo ONLY implementing GET methods ATM, and not any POST/AUTH methods.
 */
enum ArtistDTOMap: string {

  case addTags = 'addTags';
  case getCorrection = 'getCorrection';
  case getInfo = Info::class;
  case getSimilar = SimilarArtists::class;
  case getTags = 'getTags';
  case getTopAlbums = TopAlbums::class;
  case getTopTags = TopTags::class;
  case getTopTracks = TopTracks::class;
  case removeTag = 'removeTag';
  case search = Search::class;
}

final class Info {

  public function __construct(
    /** @var \Drupal\musica\API\LastFM\Entity\Artist */
    public readonly mixed $artist,
  ) {}

}

/**
 * Data transfer object for artist.getSimilar.
 *
 * @phpstan-type RootValue array{'artist': list<\Drupal\musica\API\LastFM\Entity\Artist>, "@attr"?: Attribute}
 *
 * @see https://www.last.fm/api/show/artist.getSimilar
 */
final class SimilarArtists {

  public function __construct(
    /** @var RootValue $similarartists */
    public readonly array $similarartists,
  ) {}

}

/**
 * Data transfer object for artist.getTopAlbums.
 *
 * @phpstan-type RootValue array{'album': list<\Drupal\musica\API\LastFM\Entity\Album>, "@attr"?: Attribute}
 *
 * @see https://www.last.fm/api/show/artist.getTopAlbums
 */
final class TopAlbums {

  public function __construct(
    /** @var RootValue $topalbums */
    public readonly array $topalbums,
  ) {}

}

/**
 * @phpstan-type tags array{'tag': list<\Drupal\musica\API\LastFM\Entity\Tag>, "@attr"?: Attribute}
 */
final class TopTags {

  public function __construct(
    /** @var tags $toptags */
    public readonly mixed $toptags,
  ) {}

}

/**
 * Data transfer object for artist.getTopTracks.
 *
 * @phpstan-type RootValue array{'track': list<\Drupal\musica\API\LastFM\Entity\Track>, "@attr"?: Attribute}
 */
final class TopTracks {

  public function __construct(
    /** @var RootValue $toptracks */
    public readonly mixed $toptracks,
  ) {}

}
