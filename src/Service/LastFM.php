<?php

declare(strict_types = 1);

namespace Drupal\musica\Service;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Messenger\Messenger;
use Drupal\musica\Service\ServiceInterface;
use GuzzleHttp\Client as GuzzleHttpClient;
use Psr\Http\Message\ResponseInterface;
use Drupal\Core\Extension\ExtensionList;
use Drupal\Core\File\FileSystem;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

/**
 * Hello world.
 */
class LastFM implements ServiceInterface {

  /**
   * Guzzle HTTP Client.
   */
  protected GuzzleHttpClient $client;

  /**
   * LastFM API key.
   */
  public string $apiKey;

  /**
   * LastFM private key.
   */
  protected string $apiSecret;

  /**
   * Drupal logging facilities.
   */
  protected Messenger $messenger;

  /**
   * Class constructor.
   */
  public function __construct(
      Messenger $messenger,
      ConfigFactory $configFactory
    ) {
    $this->messenger = $messenger;
    $settings = $configFactory->get('musica.settings');

    $this->client = new GuzzleHttpClient(['base_uri' => $settings->get('lastfm_api_endpoint')]);
    $this->apiKey = $settings->get('lastfm_api_key');
    $this->apiSecret = $settings->get('lastfm_api_secret');
  }

  /**
   * Step 2 - Fetch request token.
   *
   * @return string
   *   Request token.
   */
  public function fetchRequestToken() {

    $params = [
      'api_key' => $this->apiKey,
      'method' => 'auth.gettoken',
    ];
    $o = $this->sendRequest($params);
    $token = json_decode($o->getBody()->getContents());
    return $token->token;
  }

  /**
   * Step 4- Fetch  web service session key.
   */
  public function fetchSessionKey(string $request_token) {
    $session_request = [
      'api_key' => $this->apiKey,
      'method' => 'auth.getSession',
      'token' => $request_token,
    ];
    $o = $this->sendRequest($session_request);
    $session_response = json_decode($o->getBody()->getContents());
    return $session_response?->session?->key;
  }

  /**
   * {@inheritdoc}
   */
  public function request(string $namespace, string $call, array $request) {
    // Append API key to request.
    // @todo throw exception if API key is not present.
    $request = [
      ...$request,
      'api_key' => $this->apiKey,
      'method' => $namespace . '.' . $call,
    ];

    // Fetch the request specifications for the call.
    // Placeholde for https://www.drupal.org/project/musica/issues/3415226.
    // $spec = $this->serviceNsRequestParameters($this->specName, $namespace, $call);
    $spec = [];

    // Merge the spec with the user request and drop any empty parameters.
    $merged_request = array_filter([...$spec, ...$request], fn ($value) => $value !== '');

    // // @todo can the response be mapped to a typed native object instead of stdClass?
    try {
      $res = $this->sendRequest($merged_request);
      if ($res->getStatusCode() === 200) {
        return $res->getBody()->getContents();
      }
    }
    catch (\Throwable $th) {
      $this->messenger->addError($th->getMessage());
    }
    return '';
  }

  /**
   * Uses Guzzle to send and receive signed reqeust.
   *
   * @param array $parameters
   *   Parameters argument.
   */
  public function sendRequest(array $parameters = []): ResponseInterface {
    $parameters['api_sig'] = $this->sign($parameters);
    $parameters['format'] = 'json';

    // Fetch a request token.
    // See https://www.last.fm/api/desktopauth.
    $options = ['query' => $parameters];
    return $this->client->request('GET', '', $options);
  }

  /**
   * Sign and return request parameters usign secret key.
   *
   * See https://www.last.fm/api/authspec#_8-signing-calls.
   *
   * @param array $parameters
   *   Associative array of request parameters.
   *
   * @return string
   *   Signature string.
   */
  protected function sign(array $parameters = []) {
    $sig = '';
    ksort($parameters, SORT_STRING);

    foreach ($parameters as $key => $value) {
      $sig .= "{$key}{$value}";
    }

    // Append secret to signature string.
    $sig .= $this->apiSecret;
    return md5($sig);
  }

}

/**
 * Trait for matching object names to yaml file keys.
 *
 * @deprecated in musica:1.1.0 and is removed from musica:1.1.1. OAS standards.
 *
 * @see https://www.drupal.org/project/musica/issues/3415238
 */
trait YamlParametersTrait {

  /**
   * Static ExtensionList service.
   */
  public function extensionList(): ExtensionList {
    return \Drupal::service('extension.list.module');
  }

  /**
   * Static Filesystem service.
   */
  public function filesystem(): FileSystem {
    return \Drupal::service('file_system');
  }

  /**
   * Provides method parameters defined in yaml files.
   *
   * @return array
   *   Array of method parameters.
   */
  public function parameters(): array {
    $spec = $this->spec();
    $namespace = $this();

    $module_path = $this->extensionList()->getPath('musica');
    $real_path = $this->filesystem()->realpath($module_path);
    $spec_file = "{$real_path}/src/Spec/{$spec}/spec.yaml";

    try {
      $parsed_spec = Yaml::parseFile($spec_file, Yaml::PARSE_CONSTANT);
    }
    catch (ParseException $exception) {
      printf('Unable to parse the YAML string: %s', $exception->getMessage());
    }
    $method = "{$namespace}.{$this->name}";
    $spec = $parsed_spec[$method] ??= [];
    $spec = [...$spec, 'method' => $method];
    return $spec;
  }

  /**
   * Returns an array of request parameters for a given service and namespace.
   *
   * @param string $service
   *   Name of the service specification.
   * @param string $namespace
   *   Namespace of the API call.
   * @param string $call
   *   Name of the API call.
   *
   * @return array
   *   Array containing the request parameters.
   */
  public function serviceNsRequestParameters(string $service, string $namespace, string $call): array {

    $module_path = $this->extensionList()->getPath('musica');
    $real_path = $this->filesystem()->realpath($module_path);
    $spec_file = "{$real_path}/src/Spec/{$service}/spec.yaml";

    try {
      $parsed_spec = Yaml::parseFile($spec_file, Yaml::PARSE_CONSTANT);
    }
    catch (ParseException $exception) {
      printf('Unable to parse the YAML string: %s', $exception->getMessage());
    }
    $method = "{$namespace}.{$call}";
    $spec_parameters = $parsed_spec[$method] ??= [];
    $merged_spec = [...$spec_parameters, 'method' => $method];
    return $merged_spec;
  }

}
