<?php

declare(strict_types=1);

namespace Drupal\musica\Service\SoundCloud;

/**
 * SoundCloud OAuth token cache identifiers.
 */
enum AuthToken: string {
  case ACCESS = 'musica:soundcloud:access_token';
  case REFRESH = 'musica:soundcloud:refresh_token';
}
