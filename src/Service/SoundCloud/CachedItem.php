<?php

declare(strict_types=1);

namespace Drupal\musica\Service\SoundCloud;

/**
 * Type representing the standard Drupal Cache API object.
 */
class CachedItem {

  public function __construct(
    public mixed $data,
    public int $created,
    public array $tags,
    public bool $valid,
    public int $expire,
    public string $checksum,
    public int $serialized,
  ) {}

}
