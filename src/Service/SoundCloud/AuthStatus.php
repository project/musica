<?php

declare(strict_types=1);

namespace Drupal\musica\Service\SoundCloud;

// phpcs:disable Drupal.Commenting.InlineComment.DocBlock
// @see https://www.drupal.org/project/coder/issues/3283741#comment-15636100

/**
 * Stub.
 */
enum AuthStatus {

  /**
   * An OAuth code has been granted to the service and is awaiting consumption.
   */
  case AUTH_CODE_GRANTED;

  /**
   * The service possesses a valid access and/or refresh token.
   */
  case AUTHORIZED;
}
