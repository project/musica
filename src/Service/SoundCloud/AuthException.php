<?php

declare(strict_types=1);

namespace Drupal\musica\Service\SoundCloud;

use RuntimeException;

/**
 * Thrown when errors occur during service authentication.
 */
class AuthException extends RuntimeException {}
