<?php

declare(strict_types=1);

namespace Drupal\musica\Service\SoundCloud;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\Context\UserCacheContext;
use Drupal\Core\Cache\DatabaseBackend;
use Drupal\Core\Cache\MemoryBackend;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use League\OAuth2\Client\Grant\Exception\InvalidGrantException;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Token\AccessTokenInterface;
use stdClass;
use Symfony\Component\HttpFoundation\Session\Session;
use Widmogrod\Functional as f;
use Widmogrod\Monad\Either as e;
use Widmogrod\Monad\Either\Either;
use Widmogrod\Monad\Either\Left;
use Widmogrod\Monad\Either\Right;
use Widmogrod\Monad\Maybe as m;
use Widmogrod\Monad\Maybe\Maybe;

/**
 * SoundCloud Service.
 *
 * Provides OAuth athentication and API configuration facilities for the
 * SoundCloud service.
 *
 * @see https://packagist.org/packages/martin1982/oauth2-soundcloud
 */
class Auth {

  use \Drupal\Core\StringTranslation\StringTranslationTrait;

  /**
   * SoundCloud Service constructor.
   */
  public function __construct(
    private Client $client,
    private UserCacheContext $context,
    public DatabaseBackend | MemoryBackend $cache,
    private Session $session,
  ) {}

  /**
   * Return the OAuth2 provider.
   */
  public function getProvider(): Provider {
    return $this->client->provider;
  }

  /**
   * Array of cache tags.
   *
   * @param array<string> $tags
   *   Optional cache tags.
   *
   * @return array<string>
   *   Array of cache tags.
   */
  public function cacheLabels($tags = []): array {
    return array_merge(['musica', 'musica:soundcloud', $this->context->getLabel() . ':' . $this->context->getContext()], $tags);
  }

  /**
   * Revoke the application's access to the API.
   *
   * Instantly purges all access tokens from the Cache API.
   */
  public function revoke(?AuthToken $case = NULL): void {
    $invalidate = fn (AuthToken $case) => $this->cache->invalidate($case->value);
    ($case !== NULL) ? $invalidate($case) : array_map(fn (AuthToken $case) => $invalidate($case), AuthToken::cases());
  }

  /**
   * Requests authorization code from the OAuth provider.
   *
   * If an authorization code is retrieved, an HTTP redirect is sent to the
   * user for the OAuth authorization URL.
   */
  public function requestAuthCode(): Either {
    if (!isset($_GET['code'])) {

      // If we don't have an authorization code then get one.
      $authUrl = $this->client->provider->getAuthorizationUrl();
      $this->session->set('oauth2state', $this->client->provider->getState());

      // Store the PKCE code after the `getAuthorizationUrl()` call.
      // See https://oauth2-client.thephpleague.com/usage/
      $this->session->set('oauth2pkceCode', $this->client->provider->getPkceCode());
      $redirect = (new TrustedRedirectResponse(Url::fromUri($authUrl, ['absolute' => TRUE])->toString()))->setMaxAge(0);
      return Right::of($redirect);
    }
    // Check given state against previously stored one to mitigate CSRF attack.
    if (empty($_GET['state']) || ($_GET['state'] !== $this->session->get('oauth2state'))) {
      $this->session->remove('oauth2state');
    }
    return Left::of(new AuthException('Invalid OAuth authorization state.'));
  }

  /**
   * Store the given token in cache.
   *
   * @todo I think drush cr clears this bin.
   */
  public function cacheToken(AccessTokenInterface $token): void {
    // Access token expiration is given by the OAuth response.
    $this->cache->set(
      AuthToken::ACCESS->value,
      $token->getToken(),
      $token->getExpires(),
      $this->cacheLabels()
    );
    // Refresh tokens do not expire.
    $this->cache->set(
      AuthToken::REFRESH->value,
      $token->getRefreshToken(),
      Cache::PERMANENT,
      $this->cacheLabels()
    );
  }

  /**
   * Authorize the service.
   *
   * When a `$authorization_code` is provided, an access token will be requested
   * and stored in cache.
   *
   * Otherwise, this function returns `Right` of `TRUE` if a valid, non-expired
   * access token is found to exist in cache.
   *
   * If neither a new access token is requested or an existing one found, the
   * cache is queried for a refresh token.
   *
   * All operations will return `Right` of `TRUE` when valid access tokens
   * either exist or have been created.
   *
   * @return \Widmogrod\Monad\Either\Either
   *   Returns `Left` of `IdentityProviderException|Exception` for any errors
   *   and `Right` of `TRUE` when service is authorized.
   */
  public function authorize(?string $authorization_code = NULL): Either {
    $cache_token = function ($token) {
      $this->cacheToken($token);
      return Right::of(TRUE);
    };

    // An access token is being requested with a valid authorization code.
    if ($authorization_code !== NULL) {
      return $this->getAccessToken('authorization_code', ['code' => $authorization_code])->either(
        fn ($e) => Left::of($e),
        $cache_token
      );
    }

    // Application is already authorized with a non-expired token.
    if ($this->cacheGet(AuthToken::ACCESS->value) != m\nothing()) {
      return Right::of(TRUE);
    }

    return f\pipe(
      $this->cacheGet(AuthToken::REFRESH->value),
      fn (Maybe $cached) => m\fromMaybe(Left::of(new AuthException('Refresh token invalid or missing')), $cached),
      fn (Left|CachedItem|stdClass $token) =>
        $token instanceof Left ?
        $token :
        $this->getAccessToken('refresh_token', ['refresh_token' => $token->data])->either(
          fn ($e) => Left::of($e),
          $cache_token
        ),
    );
  }

  /**
   * Gets an access and refresh token with the given authorization code grant.
   *
   * For questions regarding grants, see linked documentation.
   *
   * @param string $grant
   *   OAuth2 provider grant.
   * @param array<string, mixed> $options
   *   OAuth2 provider options.
   *
   * @see https://oauth2-client.thephpleague.com/usage/
   */
  public function getAccessToken(string $grant, array $options = []): Either {
    try {
      // Test environments might not have sessions.
      if ($this->session->isStarted()) {
        // Otherwise PKCE code must exist.
        if ($this->session->get('oauth2pkceCode', FALSE) === FALSE) {
          throw new AuthException('Cannot request access without PKCE code');
        }
        $this->client->provider->setPkceCode($this->session->get('oauth2pkceCode'));
      }
      return e\Right($this->client->provider->getAccessToken($grant, $options));
    }
    catch (IdentityProviderException | InvalidGrantException | AuthException $e) {
      return e\Left($e);
    }
  }

  /**
   * Returns a single Cache API item wrapped in a monad.
   */
  public function cacheGet(string $cid): Maybe {
    $cache = fn ($item) =>
      $item != FALSE && $item->data !== NULL ?
      m\just($item) :
      m\nothing();
    return $cache($this->cache->get($cid));
  }

  /**
   * Returns the state of the service authorization status.
   */
  public function status(AuthStatus $status): bool {
    return match ($status) {
      AuthStatus::AUTH_CODE_GRANTED => array_key_exists('code', $_GET) && !empty($_GET['code']),
      AuthStatus::AUTHORIZED => e\fromRight(FALSE, $this->authorize()),
    };
  }

}
