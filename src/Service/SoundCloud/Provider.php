<?php

declare(strict_types=1);

namespace Drupal\musica\Service\SoundCloud;

use Martin1982\OAuth2\Client\Provider\SoundCloud;

/**
 * Provides the PKCE implementation missing from Martin and League packages.
 *
 * Proof Key for Code Exchange (PKCE) is somewhat supported by league, upon
 * which Martin depends on, but is not implemented by Martin.
 *
 * @see https://developers.soundcloud.com/docs
 * @see https://www.rfc-editor.org/rfc/rfc7636.html
 * @see https://oauth2-client.thephpleague.com/usage/
 * @see https://packagist.org/packages/martin1982/oauth2-soundcloud
 * @see https://packagist.org/packages/league/oauth2-client
 * @see vendor/league/oauth2-client/src/Provider/AbstractProvider.php
 */
class Provider extends SoundCloud {

  /**
   * Define missing `pkceMethod` property.
   *
   * Mentioned by the league documentation but missing in both the Martin
   * provider and league abstract provider. Trait `GuardedPropertyTrait` filters
   * missing properties.
   *
   * @var string
   *
   * @see vendor/league/oauth2-client/src/Tool/GuardedPropertyTrait.php
   */
  protected mixed $pkceMethod;

  /**
   * Returns the current PKCE method.
   *
   * Missing from both league abstract and martin providers.
   *
   * Flow `getPkceMethod <- getAuthorizationParameters <- getAuthorizationUrl`.
   *
   * @return string|null
   *   Stub.
   *
   * @see vendor/league/oauth2-client/src/Provider/AbstractProvider.php
   */
  protected function getPkceMethod() {
    return $this->pkceMethod;
  }

}
