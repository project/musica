<?php

declare(strict_types=1);

namespace Drupal\musica\Service\SoundCloud;

use CuyZ\Valinor\Mapper\Source\Source;
use CuyZ\Valinor\MapperBuilder;
use Drupal\musica\API\SoundCloud\Track;
use GuzzleHttp\Client as GuzzleHttpClient;
use Psr\Http\Message\ResponseInterface;
use Widmogrod\Functional as f;
use Widmogrod\Monad\Either as e;
use Widmogrod\Monad\Either\Either;
use Widmogrod\Monad\Either\Left;
use Widmogrod\Monad\Either\Right;
use Widmogrod\Monad\Maybe as m;

use function Widmogrod\Monad\Either\fromRight;

/**
 * Service for Spotify API.
 *
 * Provides OAuth authentication.
 */
final class Resource {

  use \Drupal\Core\StringTranslation\StringTranslationTrait;

  /**
   * Ensures all resource operations are authenticated.
   */
  public function __construct(
    public readonly Auth $service,
  ) {
    $authorized = $service->authorize();
    $authorized instanceof Left ?
      throw e\fromLeft(new AuthException($this->t('Using resource without authorization')->render()), $authorized) :
      NULL;
  }

  /**
   * Retrieves an API `$resource` and returns the hydrated DTO `$class`.
   *
   * @param \Widmogrod\Monad\Either\Either $resource
   *   Upstream API resource to retrieve.
   * @param class-string<DTO> $class
   *   The name of the Data Transfer Object class to hydrate.
   *
   * @template DTO Data Transfer Object
   */
  public function getResourceObject(Either $resource, string $class = Track::class): Either {
    return e\tryCatch(
      f\pipeline(
        fn ($r): Source => Source::json(fromRight('', $r)),
        fn ($sauce) =>
          (new MapperBuilder())
            ->allowSuperfluousKeys()
            ->allowPermissiveTypes()
            ->enableFlexibleCasting()
            ->mapper()
            ->map($class, $sauce)
      ),
      fn ($e) => $e,
    )($resource);
  }

  /**
   * Requests, caches and returns the specified API resource.
   */
  public function getResource(string $resource = 'tracks/308946187'): Either {
    $cached = m\fromMaybe(FALSE, $this->service->cacheGet($resource));
    if ($cached !== FALSE) {
      return e\right($cached);
    }

    return e\tryCatch(
      function ($resource) {
        $provider = $this->service->getProvider();
        $client = new GuzzleHttpClient(['base_uri' => $provider::BASE_SOUNDCLOUD_URL]);
        $token = m\fromMaybe(FALSE, $this->service->cacheGet(AuthToken::ACCESS->value));

        /** @var \GuzzleHttp\Psr7\Request */
        $request = $provider->getAuthenticatedRequest(
          'GET',
          $resource,
          $token->data
        );

        $response = $client->send($request);
        $content = $response->getBody()->getContents();

        $expires = $this->expires($response);
        if ($expires > 0) {
          $this->service->cache->set(
            $resource,
            $content,
            $expires + (int) microtime(TRUE),
            $this->service->cacheLabels(['musica:soundcloud:resource'])
          );
        }
        return $content;
      },
      fn ($e) => $e,
    )($resource);
  }

  /**
   * Determines response expiration by inspecting the responses' cache headers.
   *
   * @return int
   *   Defaults to zero (no cache). Otherwise returns expiration in seconds as
   *   found in the cache header.
   */
  private function expires(ResponseInterface $response): int {
    return (function ($response): Either {
      if ($response->hasHeader('cache-control')) {
        $headers = $response->getHeader('cache-control');
        if (array_key_exists(0, $headers)) {
          return Right::of($this->parseCacheTimestamp($headers[0]));
        }
      }
      return Left::of(NULL);
    })($response)->either(fn () => 0, fn ($value) => $value);
  }

  /**
   * Header cache control parser.
   */
  private function parseCacheTimestamp(string $header): int {
    $matches = [];
    if (preg_match('/.*max-age=([0-9]*)$/', $header, $matches) === 1 && array_key_exists(1, $matches)) {
      return (int) $matches[1];
    }
    else {
      return 0;
    }
  }

}
