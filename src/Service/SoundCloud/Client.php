<?php

declare(strict_types=1);

namespace Drupal\musica\Service\SoundCloud;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigBase;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannel;
use Drupal\Core\Messenger\Messenger;
use Exception;
use Widmogrod\Functional as f;
use Widmogrod\Monad\Either\Either;
use Widmogrod\Monad\Either\Left;
use Widmogrod\Monad\Either\Right;

/**
 * Service for the SoundCloud OAuth authentication provider.
 *
 * This class configures the SoundCloud OAuth provider and makes it available
 * as a Drupal service.
 *
 * @see https://packagist.org/packages/martin1982/oauth2-soundcloud
 */
class Client {

  use \Drupal\Core\StringTranslation\StringTranslationTrait;

  /**
   * The OAuth service provider.
   */
  public Provider $provider;

  /**
   * SoundCloud Service constructor.
   */
  public function __construct(
    ConfigFactory $factory,
    private LoggerChannel $logger,
    private Messenger $messenger,
  ) {
    $this->validateConfig($this->configFromEnv($factory->getEditable('musica.settings')))
      ->either(
        fn ($e) => $this->log($e),
        function (ConfigBase $config) {
          $options = [
            'redirectUri'  => $config->get('soundcloud_redirect_url'),
            'clientId'     => $config->get('soundcloud_client_id'),
            'clientSecret' => $config->get('soundcloud_client_secret'),
            'pkceMethod' => Provider::PKCE_METHOD_S256,
          ];
          $this->provider = new Provider($options, []);
        }
      );
  }

  /**
   * Add configuration from environment variables, if available.
   */
  public function configFromEnv(Config $config): ConfigBase {
    array_map(
      fn ($key) => $config->set($key, getenv($key) ? getenv($key) : $config->get($key)),
      ['soundcloud_client_id', 'soundcloud_client_secret']
    );
    $config->save();
    return $config;
  }

  /**
   * Checks there are no empty configuration items.
   */
  private function validateConfig(ConfigBase $config): Either {
    $emptyConfigs = f\pipeline(
      fn ($all) => array_filter($all, fn ($v) => !is_array($v)),
      fn ($r) => array_filter($r, fn ($v, $k) => preg_match('/soundcloud_*/', $k), ARRAY_FILTER_USE_BOTH),
      fn ($r) => array_filter($r, fn ($v) => empty($v))
    )($config->get());

    return empty($emptyConfigs) ?
      Right::of($config) :
      Left::of(new Exception(sprintf('Invalid configuration: "%s"',
        implode(', ', array_keys($emptyConfigs))
      )));
  }

  /**
   * Log an error in both watchdog and user interface.
   */
  private function log(\Exception $e): void {
    $s = $e->getMessage();
    $this->messenger->addMessage($s, $this->messenger::TYPE_ERROR);
    $this->logger->error($s);
  }

}
