<?php

declare(strict_types = 1);

namespace Drupal\musica\Service;

use Drupal\Core\Cache\DatabaseBackend as Cache;
use Drupal\Core\Cache\Context\UserCacheContext;
use Drupal\Core\Config\ConfigFactory;
use Kerox\OAuth2\Client\Provider\Spotify as SpotifyClient;
use Kerox\OAuth2\Client\Provider\SpotifyScope;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Drupal\Core\Logger\LoggerChannel;
use Drupal\Core\Utility\Error;
use Drupal\musica\API\Spotify\Entity\Artist;
use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;

use CuyZ\Valinor\Mapper\Source\Source;
use CuyZ\Valinor\MapperBuilder;

/**
 * Service for Spotify API.
 *
 * Provides OAuth authentication.
 */
final class Spotify {

  private bool $authorized;
  private string $endpoint;
  private array $cacheLabels;
  private Cache $cache;
  private LoggerChannel $logger;
  private SpotifyClient $provider;

  /**
   * Spotify Service constructor.
   */
  public function __construct(
    Cache $cache,
    UserCacheContext $context,
    LoggerChannel $logger,
    ConfigFactory $configFactory
    ) {
    $this->cache = $cache;
    $this->logger = $logger;

    $settings = $configFactory->get('musica.settings');
    $this->endpoint = $settings->get('spotify_api_endpoint');
    $this->provider = new SpotifyClient([
      'redirectUri'  => $settings->get('spotify_redirect_url'),
      'clientId'     => $settings->get('spotify_client_id'),
      'clientSecret' => $settings->get('spotify_client_secret'),
    ]);

    $this->cacheLabels = [$context->getLabel() . ':' . $context->getContext()];

    // Refresh access token at service instatiation.
    $this->authorized = $this->setAccess();
  }

  /**
   * Header cache control parser.
   */
  private function parseCacheTimestamp(string $header): int {
    $matches = [];
    if (preg_match('/.*max-age=([0-9]*)$/', $header, $matches) === 1 && array_key_exists(1, $matches)) {
      return (int) $matches[1];
    }
    else {
      return 0;
    }
  }

  /**
   * Determines response expiration by inspecting the responses' cache headers.
   *
   * @return int
   *   Defaults to zero (no cache). Otherwise returns expiration in seconds as
   *   found in the cache header.
   */
  private function expires(Response $response): int {
    $expires = 0;
    if ($response->hasHeader('cache-control')) {
      $headers = $response->getHeader('cache-control');
      if (array_key_exists(0, $headers)) {
        $expires = $this->parseCacheTimestamp($headers[0]);
      }
    }
    return $expires;
  }

  /**
   * Retrieves an API `$resource` and returns the hydrated DTO `$class`.
   *
   * @TODO: still considering hydration alternatives to Valinor.
   *
   * @template DTO Data Transfer Object
   *
   * @param class-string<DTO> $class
   *   The name of the Data Transfer Object class to hydrate.
   * @param string $resource
   *   Upstream API resource to retrieve.
   *
   * @return DTO|null
   *   The fully hydrated instance of `DTO` or `NULL`.
   */
  public function getResourceObject(string $class = Artist::class, string $resource = 'artists/0TnOYISbd1XYRBk9myaseg') {
    $dto = NULL;
    $data = $this->getResource($resource);

    try {
      $sauce = Source::json($data);
      /** @var DTO */
      $dto = (new MapperBuilder())
        ->allowSuperfluousKeys()
        ->allowPermissiveTypes()
        ->enableFlexibleCasting()
        ->mapper()
        ->map($class, $sauce);
    }
    catch (\Throwable $e) {
      Error::logException($this->logger, $e);
    }
    return $dto;
  }

  /**
   * Requests, caches and returns the specified API resource.
   */
  public function getResource(string $resource = 'artists/4Z8W4fKeB5YxbusRsdQVPb'): string {

    // Serve the requested resource from cache if it is available.
    if ($this->cache->get($resource) !== FALSE) {
      return $this->cache->get($resource)->data;
    }

    // Make sure service has valid access before calling the API.
    if (!$this->authorized) {
      throw new \Exception("Musica: Service is not authorized to access Spotify API.", 1);
    }

    $content = '';
    $client = new GuzzleHttpClient(['base_uri' => $this->endpoint]);

    /** @var \GuzzleHttp\Psr7\Request */
    $request = $this->provider->getAuthenticatedRequest(
      'GET',
      $resource,
      $this->getToken()
    );

    try {
      $response = $client->send($request);
      $content = $response->getBody()->getContents();

      $expires = $this->expires($response);
      if ($expires > 0) {
        $this->cache->set(
          $resource,
          $content,
          $expires + microtime(TRUE),
          ['musica', 'musica:artist']
        );
      }
    }
    catch (GuzzleException $e) {
      Error::logException($this->logger, $e);
    }

    return $content;
  }

  public function getToken() {
    $token = $this->cache->get('musica:spotify:access_token');
    if ($token !== FALSE) {
      return $token->data;
    }
  }

  /**
   * Sets the service authorization state.
   *
   * If the access token is invalid a new one is requested using an existing
   * refresh token, if it exists.
   */
  private function setAccess(): bool {
    // Application is already authorized with a non-expired token.
    if ($this->cache->get('musica:spotify:access_token') !== FALSE) {
      return TRUE;
    }

    // Re-authorize application if a refresh token is available.
    $refresh = $this->cache->get('musica:spotify:refresh_token');
    if ($refresh !== FALSE && $refresh->data !== NULL) {
      return $this->getAccessToken('refresh_token', [
        'refresh_token' => $refresh->data,
      ]);
    }

    return FALSE;
  }

  /**
   * Authorizes the service to use the Spotify API.
   *
   * @see https://developer.spotify.com/documentation/web-api/tutorials/refreshing-tokens
   */
  public function authorize(): void {

    if (!isset($_GET['code'])) {
      // If we don't have an authorization code then get one.
      $authUrl = $this->provider->getAuthorizationUrl([
        'scope' => [
          SpotifyScope::USER_READ_EMAIL->value,
          SpotifyScope::USER_TOP_READ->value,
          SpotifyScope::USER_READ_PLAYBACK_STATE->value,
          SpotifyScope::USER_MODIFY_PLAYBACK_STATE->value,
          SpotifyScope::USER_READ_CURRENTLY_PLAYING->value,
          SpotifyScope::USER_READ_PLAYBACK_POSITION->value,
          SpotifyScope::USER_READ_RECENTLY_PLAYED->value,
          SpotifyScope::STREAMING->value,
          SpotifyScope::PLAYLIST_READ_PRIVATE->value,
          SpotifyScope::USER_LIBRARY_MODIFY->value,
          SpotifyScope::USER_LIBRARY_READ->value,
        ],
      ]);
      // $this->cache->set('musicaSpotifyAuthState', $this->provider->getState(), Cache::CACHE_PERMANENT, $this->cacheLabels);

      header('Location: ' . $authUrl);
      exit;
    }
    else {
      $this->authorized = $this->getAccessToken('authorization_code', ['code' => $_GET['code']]);
    }
  }

  /**
   * Gets an access and refresh token with the given authorization code grant.
   *
   * The access token is stored in cache and set to expire to the lifetime
   * given by the upstream API. The refresh token does not expire.
   *
   * @return bool
   *   Returns TRUE the access token was successfully retrieved and cached,
   *   FALSE if there was an authentication error.
   */
  private function getAccessToken(string $grant, array $options = []): bool {
    try {
      $token = $this->provider->getAccessToken($grant, $options);

      $this->cache->set(
        'musica:spotify:access_token',
        $token->getToken(),
        $token->getExpires(),
        $this->cacheLabels
      );

      $this->cache->set(
        'musica:spotify:refresh_token',
        $token->getRefreshToken(),
        Cache::CACHE_PERMANENT,
        $this->cacheLabels
      );

      return TRUE;
    }
    catch (IdentityProviderException $e) {
      Error::logException($this->logger, $e, 'Spotify authentication failed: @error', [
        '@error' => $e->getMessage(),
      ]);
    }
    return FALSE;
  }

}
