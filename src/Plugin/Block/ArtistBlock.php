<?php

declare(strict_types = 1);

namespace Drupal\musica\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a 'Artist' Block.
 */
#[Block(
  id: 'musica:artist',
  admin_label: new TranslatableMarkup('Artist Block'),
  category: new TranslatableMarkup('Musica'),
)]
class ArtistBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      'artist' => [
        'variables' => [],
      ],
    ];
  }

}
