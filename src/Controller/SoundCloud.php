<?php

declare(strict_types=1);

namespace Drupal\musica\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\musica\API\SoundCloud\Track;
use Drupal\musica\Service\SoundCloud\Auth as Service;
use Drupal\musica\Service\SoundCloud\Resource;
use Symfony\Component\DependencyInjection\ContainerInterface;

use function Widmogrod\Monad\Either\fromLeft;
use function Widmogrod\Monad\Either\fromRight;

/**
 * Stub.
 */
final class SoundCloud extends ControllerBase {

  /**
   * Service.
   */
  protected Service $service;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->service = $container->get('musica.soundcloud.auth');
    return $instance;
  }

  /**
   * Stub.
   */
  public function test(): mixed {

    $a = new Resource($this->service);
    $b = $a->getResource('tracks/308946187');
    $either = $a->getResourceObject($b);

    $error = fromLeft(FALSE, $either);
    if ($error) {
      $this->messenger()->addError($error->getMessage());
    }
    $dto = (fn ($r): Track => fromRight(FALSE, $r))($either);

    $track = $this->t(
      'Title: @title, Description: @desc, Genre: @genre, User: @user',
      [
        '@title' => $dto->title,
        '@desc' => $dto->description,
        '@genre' => $dto->genre,
        '@uri' => $dto->uri,
        '@user' => $dto->user->permalink,
      ]
    );

    return [
      '#type' => 'markup',
      '#markup' => $track->render(),
    ];
  }

}
