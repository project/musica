<?php

declare(strict_types = 1);

namespace Drupal\musica\Form;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configurtion form for SoundCloud service.
 *
 * @see https://www.drupal.org/docs/drupal-apis/services-and-dependency-injection/dependency-injection-for-a-form
 * @see https://www.drupal.org/docs/develop/creating-modules/defining-and-using-your-own-configuration-in-drupal
 */
final class SoundCloudConfig extends FormBase {

  private Config $config;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'musica_config_soundcloud';
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactory $configFactory) {
    $this->config = $configFactory->getEditable('musica.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['about'] = [
      '#type' => 'item',
      '#title' => $this->t('SoundCloud® API'),
      '#description' => $this->t('For more information about the SoundCloud API visit the <a href="https://developers.soundcloud.com/">SoundCloud Developer Portal</a>'),
    ];

    $form['description'] = [
      '#type' => 'item',
      '#title' => $this->t('Configuration'),
      '#description' => $this->t('This page lets you configure the various parameters you need in order to connect to the SoundCloud API.<br />
      You can find your Client ID and Client Secret in the <a href="https://soundcloud.com/you/apps">https://soundcloud.com/you/apps</a> page.<br /><br />
      If you need to update your Client ID, Secret, or redirect URL contact the SoundCloud team via their <a href="https://github.com/soundcloud/api/">issue tracker</a>.'),
    ];

    $form['soundcloud_redirect_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redirect URL'),
      '#description' => $this->t('OAuth2 redirection endpoint. Your app redirects a user to this endpoint after the authorization is complete'),
      '#default_value' => empty($form_state->get('soundcloud_redirect_url')) ?
      $this->config->get('soundcloud_redirect_url') : $form_state->get('soundcloud_redirect_url'),
    ];

    $form['soundcloud_client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#description' => $this->t('The service client id'),
      '#default_value' => empty($form_state->get('soundcloud_client_id')) ?
      $this->config->get('soundcloud_client_id') : $form_state->get('soundcloud_client_id'),
    ];

    $form['soundcloud_client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client secret'),
      '#description' => $this->t('The service client secret'),
      '#default_value' => empty($form_state->get('soundcloud_client_secret')) ?
      $this->config->get('soundcloud_client_secret') : $form_state->get('soundcloud_client_secret'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config->set(
      'soundcloud_redirect_url',
      $form_state->getValue('soundcloud_redirect_url')
    )->set(
      'soundcloud_client_id',
      $form_state->getValue('soundcloud_client_id')
    )->set(
      'soundcloud_client_secret',
      $form_state->getValue('soundcloud_client_secret')
    )->save();
  }

}
