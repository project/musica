<?php

declare(strict_types=1);

namespace Drupal\musica\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\State;
use Drupal\musica\Service\SoundCloud\Auth as SoundCloud;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Authorization form for SoundCloud service.
 *
 * @see https://www.drupal.org/docs/drupal-apis/services-and-dependency-injection/dependency-injection-for-a-form
 * @see https://www.drupal.org/docs/develop/creating-modules/defining-and-using-your-own-configuration-in-drupal
 */
class SoundCloudConsent extends FormBase {

  /**
   * State API item id for `terms` form element.
   */
  const TOS_CONSENT = 'musica:soundcloud:tos_consent';
  /**
   * Form state.
   */
  protected State $state;
  /**
   * SoundCloud service.
   */
  protected SoundCloud $service;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'musica_auth_soundcloud';
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(State $state, SoundCloud $service) {
    $this->state = $state;
    $this->service = $service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('state'),
      $container->get('musica.soundcloud.auth')
    );
  }

  /**
   * Whether the user has consented to the SoundCloud Terms of Use or not.
   */
  protected function hasConsent(): bool {
    return $this->state->get(self::TOS_CONSENT) ?? FALSE;
  }

  /**
   * Form consent element.
   *
   * @return array<string, mixed>
   *   Form API element.
   */
  protected function consentElement(): array {
    if ($this->hasConsent()) {
      return [];
    }
    else {
      return [
        'terms' => [
          '#type' => 'item',
          '#title' => $this->t('SoundCloud® Terms of Use'),
          '#description' => $this->t('<b>NOTICE</b>: In order to use the SoundCloud API you must first consent to their <a href="/admin/config/musica/soundcloud/consent">Terms of Use</a>'),
          '#required' => TRUE,
        ],
      ];
    }
  }

  /**
   * Form builder.
   *
   * @param array<mixed> $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing the current state of a form.
   *
   * @see https://api.drupal.org/api/drupal/elements/10
   * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Render%21Element%21FormElement.php/class/FormElement/10
   *
   * @return array<mixed>
   *   Form API form array.
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    // If the user already consented to the TOS, gray out the element.
    $consented = $this->hasConsent();
    $consent = $form_state->hasValue('terms') ? $form_state->get('terms') : FALSE;

    $build = array_merge($form, [
      'description' => [
        '#type' => 'item',
        '#title' => $this->t('SoundCloud® Terms of Use'),
        '#description' => $this->t('For more information about SoundCloud\'s Terms of Use visit  <a href="https://developers.soundcloud.com/docs/api/terms-of-use">https://developers.soundcloud.com/docs/api/terms-of-use</a>'),
      ],
      'terms' => [
        '#type' => 'checkbox',
        '#title' => $this->t('I have fully read and agree to follow SoundCloud\'s <a href="https://developers.soundcloud.com/docs/api/terms-of-use">API Terms of Use</a>.'),
        '#description' => $this->t('<b>NOTICE</b>: You will not be allowed to configure and use the application until you have read and agreed to the Terms of Use linked above.'),
        '#return_value' => TRUE,
        '#required' => TRUE,
        '#default_value' => $consented ? $consented : $consent,
        '#disabled' => $consented ? $consented : $consent,
      ],
      'actions' => [
        '#type' => 'actions',
        'submit' => [
          '#type' => 'submit',
          '#value' => $this->t('Consent'),
          '#button_type' => 'primary',
          '#disabled' => $consented ? $consented : $consent,
        ],
      ],
    ]);
    return $build;
  }

  /**
   * Form validate handler.
   *
   * @param array<mixed> $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing the current state of a form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {

    // The #required property requires consent to proceed, this is a backup.
    if ($form_state->getValue('terms') !== TRUE) {
      $form_state->setErrorByName(
        'auth_confirmation',
        $this->t('YOU MUST READ AND AGREE TO SOUNDCLOUD\'s "API TERMS OF USE" BEFORE PROCEEDING.')->render()
      );
    }
  }

  /**
   * Form submit handler.
   *
   * @param array<mixed> $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing the current state of a form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->state->set(self::TOS_CONSENT, $form_state->getValue('terms'));
  }

}
