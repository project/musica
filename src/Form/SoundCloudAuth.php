<?php

declare(strict_types=1);

namespace Drupal\musica\Form;

use DateTimeImmutable;
use DateTimeZone;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\musica\Service\SoundCloud\AuthStatus as Status;
use Drupal\musica\Service\SoundCloud\AuthToken;
use Drupal\musica\Service\SoundCloud\CachedItem;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Widmogrod\Monad\Maybe as m;

/**
 * Authorization form for SoundCloud service.
 */
final class SoundCloudAuth extends SoundCloudConsent {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'musica_soundcloud_auth';
  }

  /**
   * Form element with current authorization status.
   *
   * @return array<mixed>
   *   FAPI element.
   */
  private function displayTokenItem(AuthToken $type): array {
    $element = [];

    $label = fn () => match ($type) {
      AuthToken::ACCESS => $this->t('access'),
      AuthToken::REFRESH => $this->t('refresh'),
    };

    $text = fn (CachedItem|\stdClass|FALSE $token) => $token === FALSE ?
      $this->t('No token information available') :
      $this->t(
      'Your @type token does @expire', [
        '@type' => $label(),
        '@expire' => match ($type) {
          AuthToken::REFRESH => $this->t('not expire'),
          AuthToken::ACCESS => $this->t('expire on') . ' ' . (new DateTimeImmutable('now', new DateTimeZone('America/New_York')))
            ->setTimestamp((int) $token->expire)
            ->format(DateTimeImmutable::COOKIE),
        },
      ]);

    if ($this->service->status(Status::AUTHORIZED)) {
      $element = [
        '#type' => 'item',
        '#title' => $this->t("@type token", ['@type' => ucfirst(strtolower($type->name))]),
        '#description' => $text(m\fromMaybe(FALSE, $this->service->cacheGet($type->value))),
      ];
    }

    return $element;
  }

  /**
   * Terminate request with a redirect response.
   *
   * Response objects are being deprecated in form builders, and using
   * form_state->setRedirect(), setResponse() has no effect on build return.
   *
   * Credit @larowlan, @djg_tram, et. al.
   *
   * @see https://www.drupal.org/project/redirect_after_login/issues/3214949
   * @see https://drupal.stackexchange.com/a/303653/693
   * @see https://www.drupal.org/project/drupal/issues/2363189
   */
  protected function sendRedirect(Url $url): void {
    $res = new RedirectResponse($url->toString());
    $req = $this->getRequest();
    $req->getSession()->save();
    $res->prepare($req);
    /** @var \Drupal\Core\DrupalKernel $kernel */
    $kernel = \Drupal::service('kernel');
    $kernel->terminate($req, $res);
    $res->send();
  }

  /**
   * This Form authorizes the application to use the Spotify API.
   *
   * @see https://api.drupal.org/api/drupal/elements/10
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $authorized = $this->service->status(Status::AUTHORIZED);

    $auth_success = function () {
      $this->messenger()->addStatus($this->t("Your application is now authorized."));
      $this->sendRedirect(Url::fromRoute('musica.config_soundcloud_authorize'));
    };

    if ($this->service->status(Status::AUTH_CODE_GRANTED)) {
      $this->service->authorize($_GET['code'])->either(
        fn ($e) => $this->messenger()->addError($this->t(
          'Authorization failed due to <em>@e</em>', ['@e' => $e->getMessage()]
        )),
        $auth_success,
      );
    }

    $form['about'] = [
      '#type' => 'item',
      '#title' => $this->t('SoundCloud® API'),
      '#description' => $this->t('For more information about the SoundCloud API visit the <a href="https://developers.soundcloud.com/">SoundCloud Developer Portal</a>'),
    ];

    $form['description'] = [
      '#type' => 'item',
      '#title' => $this->t('Authorization'),
      '#description' => $this->t('This page allows your Drupal application to connect with the SoundCloud API using the parameters you entered in the <a href="/admin/config/musica/soundcloud/configure">Configure</a> page.'),
    ];

    $text_not_authorized = $this->t('Your application is currently not authorized. </br>
      To authorize your application click on the "Authorize" button. </br>
      <b>Notice: You will be automatically re-directed to SoundCloud for authorization.</b>');
    $text_authorized = $this->t('Your application is currently authorized.');

    $form['authorized'] = [
      '#type' => 'item',
      '#title' => $this->t('Status'),
      '#description' => $authorized ? $text_authorized : $text_not_authorized,
      '#return_value' => $authorized,
      '#default_value' => $authorized,
      '#disabled' => !$authorized,
    ];

    $form['access_token'] = $this->displayTokenItem(AuthToken::ACCESS);
    $form['refresh_token'] = $this->displayTokenItem(AuthToken::REFRESH);

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Authorize'),
      '#button_type' => 'primary',
      '#disabled' => $authorized,
    ];
    $form['actions']['revoke'] = [
      '#type' => 'button',
      '#value' => $this->t('Revoke'),
      '#disabled' => !$authorized,
    ];
    return $form;
  }

  /**
   * Form validator.
   *
   * @param array<mixed> $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {

    // If the form is NOT "submitted" but it is being validated, it means the
    // revoke action has been invoked.
    if (!$form_state->isSubmitted()) {
      $this->service->revoke();
      $this->messenger()->addWarning($this->t("Musica has revoked your application's SoundCloud authorization.</br>
        You may still request re-authorization, if so desired."));
    }
  }

  /**
   * Requests OAuth authorization code.
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->service->requestAuthCode()->either(
      fn ($e) => $this->messenger()->addError(
        $this->t('Authorization failed due to <em>@e</em>', ['@e' => $e->getMessage()])
      ),
      fn ($r) => $form_state->setResponse($r)
    );
    $form_state->setRebuild();
  }

}
