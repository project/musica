<?php

/**
 * @file
 * Música's collection of Data Transfer Objects.
 *
 * @see https://api-platform.com/docs/schema-generator/
 */

declare(strict_types = 1);

namespace Drupal\musica\API\LastFM\Entity;

use Drupal\musica\API\LastFM\Ontology\MusicArtist;
use Drupal\musica\API\LastFM\Utility\Images;
use Drupal\musica\API\LastFM\Utility\Bio;

/**
 * @see http://purl.org/ontology/mo/MusicArtist
 *
 * @phpstan-type taglist array{tag?: list<Tag>}
 * @phpstan-type getInfoSimilar array{artist?: list<Artist>}
 */
class Artist extends MusicArtist {

  public ?Images $image;

  /**
   * Number of listeners on the platform.
   */
  public ?int $listeners = NULL;

  /**
   * Float-like string describing match ratio for search results or similar artists.
   */
  public ?string $match = NULL;

  /**
   * Musicbrainz id for the artist.
   */
  public ?string $mbid = NULL;

  /**
   * The artist name.
   */
  public string $name;

  /**
   * Boolean-like number indicating whether the artist is on tour.
   */
  public ?int $ontour = NULL;

  /**
   * @var getInfoSimilar
   */
  public ?array $similar = [];

  /**
   * @var array
   */
  public ?array $stats = [];

  /**
   * (deprecated) LastFM doest no support streaming.
   */
  public ?string $streamable = NULL;

  /**
   * @var taglist
   */
  public ?array $tags = [];

  /**
   * The URL for the artist page.
   */
  public ?string $url = NULL;

  /**
   * Wiki metadata associated with the resource.
   */
  public ?Bio $bio = NULL;

}
