<?php

/**
 * @file
 * Música's collection of Data Transfer Objects.
 *
 * @see https://api-platform.com/docs/schema-generator/
 */

declare(strict_types = 1);

namespace Drupal\musica\API\LastFM\Entity;

use Drupal\musica\API\LastFM\Ontology\Record;

/**
 * @see http://purl.org/ontology/mo/album
 */
class Album extends Record {

  /**
   * Name of the artit.
   */
  public ?Artist $artist = NULL;

  /**
   * Musicbrainz id.
   */
  public ?string $id = NULL;

  /**
   * @var array (Deprecated) Fake array of images containing empty placeholders
   */
  public ?array $image = [];

  /**
   * A number-like string.
   */
  public ?string $listeners = NULL;

  /**
   * The musicbrainz id for the resource.
   */
  public ?string $mbid = NULL;

  /**
   * Name of the album.
   */
  public string $name;

  /**
   * A number-like string.
   */
  public ?string $playcount = '';

  /**
   * A date-like string.
   */
  public ?string $released = NULL;

  /**
   * @var array An array of tags associated with the album
   */
  public ?array $toptags = [];

  /**
   * @var Track[] An array of tracks associated with the album
   */
  public ?array $tracks = [];

  /**
   * The URL for the resource.
   */
  public ?string $url = NULL;

}
