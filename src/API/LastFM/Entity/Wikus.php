<?php

declare(strict_types = 1);

/**
 * @file
 * Música's collection of Data Transfer Objects.
 *
 * @see https://api-platform.com/docs/schema-generator/
 */

namespace Drupal\musica\API\LastFM\Entity;

/**
 * Wiki property that is embedded in some resources.
 */
class Wikus {

  /**
   * Content.
   */
  public ?string $content = NULL;

  /**
   * A datetime-like string.
   */
  public ?string $published = NULL;

  /**
   * Short summary.
   */
  public ?string $summary = NULL;

}
