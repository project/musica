<?php

/**
 * @file
 * Música's collection of Data Transfer Objects.
 *
 * @see https://api-platform.com/docs/schema-generator/
 */

declare(strict_types = 1);

namespace Drupal\musica\API\LastFM\Entity;

/**
 * @see https://www.bbc.co.uk/ontologies/creativework/tag
 */
class Tag {

  /**
   * Name of the tag.
   */
  public string $name;

  /**
   * Boolean-like number (deprecated).
   */
  public ?string $streamable = NULL;

  /**
   * Number of time the tag has been used.
   */
  public ?string $taggings = NULL;

  /**
   * Page for the tag on LastFM.
   */
  public ?string $url = NULL;

  /**
   * Wiki metadata associated with the resource.
   */
  public ?Wikus $wiki = NULL;

}
