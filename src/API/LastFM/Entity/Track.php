<?php

/**
 * @file
 * Música's collection of Data Transfer Objects.
 *
 * @see https://api-platform.com/docs/schema-generator/
 */

declare(strict_types = 1);

namespace Drupal\musica\API\LastFM\Entity;

use Drupal\musica\API\LastFM\Ontology\MusicalManifestation;

/**
 * A track on a particular record.
 *
 * @see http://purl.org/ontology/mo/Track
 */
class Track extends MusicalManifestation {

  /**
   * Album associated with the track resource.
   */
  public ?Album $album = NULL;

  /**
   * Artist associated with the track resource.
   */
  public ?Artist $artist = NULL;

  /**
   * The duration of a track or a signal in ms.
   *
   * @see http://purl.org/ontology/mo/duration
   */
  public ?string $duration = NULL;

  /**
   * A number-like string referencing an internal LastFM ID.
   */
  public ?string $id = NULL;

  /**
   * A number-like string.
   */
  public ?string $listeners = NULL;

  /**
   * The musicbrainz id for the resource.
   */
  public ?string $mbid = NULL;

  /**
   * Name of the track resource.
   */
  public string $name;

  /**
   * A number-like string.
   */
  public ?string $playcount = '';

  /**
   * Associates the side on a vinyl record, where a track is located, e.g. A, B, C, etc. This property can then also be used in conjunction with mo:track\_number, so that one can infer e.g. "A1", that means, track number 1 on side A.
   *
   * @see http://purl.org/ontology/mo/record_side
   */
  public ?string $record_side = NULL;

  /**
   * (deprecated) LastFM doest no support streaming.
   */
  public ?string $streamable = NULL;

  /**
   * @var array an array of tags associated with the resource
   */
  public ?array $toptags = [];

  /**
   * Indicates the position of a track on a record medium (a CD, etc.).
   *
   * @see http://purl.org/ontology/mo/track_number
   */
  public ?int $track_number = NULL;

  /**
   * The URL for the resource.
   */
  public ?string $url = NULL;

  /**
   * Wiki metadata associated with the resource.
   */
  public ?Wikus $wiki = NULL;

}
