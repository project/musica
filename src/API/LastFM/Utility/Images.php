<?php

declare(strict_types = 1);

namespace Drupal\musica\API\LastFM\Utility;

final class Images {

  public array $index = [];

  public function __construct(
    /** @var list<ImageProps> */
    public readonly array $images = [],
  ) {
    array_walk(
      $images,
      fn (ImageProps $image) => $this->index[empty($image->size) ? 'default' : $image->size] = $image->text
    );
  }

}
