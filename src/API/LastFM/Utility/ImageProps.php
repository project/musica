<?php

declare(strict_types = 1);

namespace Drupal\musica\API\LastFM\Utility;

class ImageProps {

  public readonly string $size;

  public readonly string $text;

  public function __construct(...$args) {
    [$this->text, $this->size] = $args;
  }

}
