<?php

declare(strict_types = 1);

namespace Drupal\musica\API\LastFM\Utility;

/**
 * Data transfer object for artist.search.
 *
 * @phpstan-type RootValue array{
 *  artistmatches: array{artist: list<Artist>},
 *  "opensearch:Query": array,
 *  "opensearch:totalResults": int,
 *  "opensearch:startIndex": int,
 *  "opensearch:itemsPerPage": int,
 *  "@attr"?: Attribute
 * }
 *
 * @see https://www.last.fm/api/show/artist.search
 */
final class Search {

  public function __construct(
    /** @var RootValue $results */
    public readonly mixed $results,
  ) {}

}
