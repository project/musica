<?php

declare(strict_types = 1);

namespace Drupal\musica\API\LastFM\Utility;

class Attribute {

  public function __construct(
    public readonly string $artist = '',
    public readonly string $page = '',
    public readonly string $perPage = '',
    public readonly string $totalPages = '',
    public readonly string $total = '',
    public readonly string $for = '',
  ) {}

}
