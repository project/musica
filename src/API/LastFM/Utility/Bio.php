<?php

declare(strict_types = 1);

namespace Drupal\musica\API\LastFM\Utility;

/**
 * @phpstan-type linkList array{link?: array{"#text"?: string, rel?: string, href?: string}}
 */
final class Bio {

  public function __construct(
    public readonly string $content = '',
    public readonly string $summary = '',
    public readonly string $published = '',
    /** @var linkList */
    public readonly array $links = [],
  ) {}

}
