<?php

declare(strict_types = 1);

/**
 * @file
 * Música's collection of Data Transfer Objects.
 *
 * @see https://api-platform.com/docs/schema-generator/
 */

namespace Drupal\musica\API\LastFM\Ontology;

/**
 * An agent (eg. person, group, software or physical artifact).
 *
 * @see http://xmlns.com/foaf/0.1/Agent
 */
abstract class Agent {

}
