<?php

declare(strict_types = 1);

/**
 * @file
 * Música's collection of Data Transfer Objects.
 *
 * @see https://api-platform.com/docs/schema-generator/
 */

namespace Drupal\musica\API\LastFM\Ontology;

/**
 * A published record (manifestation which first aim is to render the product of a recording).
 *
 * @see http://purl.org/ontology/mo/Record
 */
abstract class Record extends MusicalManifestation {

}
