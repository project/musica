<?php

declare(strict_types = 1);

/**
 * @file
 * Música's collection of Data Transfer Objects.
 *
 * @see https://api-platform.com/docs/schema-generator/
 */
namespace Drupal\musica\API\LastFM\Ontology;

/**
 * A person or a group of people (or a computer :-) ), whose musical creative work shows sensitivity and imagination.
 *
 * @see http://purl.org/ontology/mo/MusicArtist
 */
abstract class MusicArtist extends Agent {

}
