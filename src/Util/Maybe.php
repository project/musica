<?php

declare(strict_types=1);

namespace Drupal\musica\Util;

// use Drupal\musica\Util\Functor;

// phpcs:disable


/**
 * Maybe.
 */
// #[Functor('event')]
abstract class Maybe {

  public static function just(mixed $value): Just {
    return new Just($value);
  }

  public static function nothing(): Nothing {
    // Implemented as a singleton (can only ever hold one value), since it
    // will never hold any values.
    return Nothing::get();
  }

  public static function fromValue(mixed $value, mixed $nullValue = null): Just | Nothing {
    return $value == $nullValue ?
      self::nothing() :
      self::just($value);
  }

  abstract public function isJust(): bool;
  abstract public function isNothing(): bool;

  /**
   * Force anyone wanting to get a value to also pass a default that will
   * be returned if there is no value. Enforces that a valid value is returned
   * even in the case of error.
   */
  abstract public function getOrElse(mixed $default): mixed;

  abstract public function map(callable $f): Maybe;
  abstract public function orElse(Maybe $m): Maybe;
  abstract public function flatMap(callable $f): Maybe;
  abstract public function filter(callable $f): Maybe;
}


final class Just extends Maybe {
  private mixed $value;
  public function __construct(mixed $value) {
    $this->value = $value;
  }
  public function isJust(): bool {
    return true;
  }
  public function isNothing(): bool {
    return false;
  }
  public function getOrElse(mixed $default): mixed {
    return $this->value;
  }
  /**
   * Applies a callable to the value but with a Maybe wrap.
   */
  public function map(callable $f): Maybe {
    return new self($f($this->value));
  }
  public function orElse(Maybe $m): Maybe {
    return $this;
  }
  /**
   * Applies a callable to the value but without a Maybe wrap.
   */
  public function flatMap(callable $f): Maybe {
    return $f($this->value);
  }
  public function filter(callable $f): Maybe {
    return $f($this->value) ? $this : Maybe::nothing();
  }
}

final class Nothing extends Maybe {
  private static mixed $instance = null;
  public static function get(): self {
    if (is_null(self::$instance)) {
      self::$instance = new static();
    }
    return self::$instance;
  }
  public function isJust(): bool {
    return false;
  }
  public function isNothing(): bool {
    return true;
  }
  public function getOrElse(mixed $default): mixed {
    return $default;
  }
  public function map(callable $f): Maybe {
    return $this;
  }
  public function orElse(Maybe $m): Maybe {
    return $m;
  }
  public function flatMap(callable $f): Maybe {
    return $this;
  }
  public function filter(callable $f): Maybe {
    return $this;
  }
}

/**
 * Wraps a function with a callable that accepts Maybe as parameters.
 */
function lift2(callable $f): callable {
  return fn () =>
    // This fold op first checks all the arguments are valid.
    array_reduce(
      func_get_args(),
      fn (bool $status, Maybe $m) => $m->isNothing() ? false : $status,
      true
    ) ?
      // If the criteria above is met, the callable is wrapped in a Just type.
      Maybe::just(call_user_func($f,
        array_map(fn (Maybe $m) => $m->getOrElse(null), func_get_args())
      )) :
      // Otherwise there's nothing to wrap and a Nothing is safely returned.
    Maybe::nothing();
}

function lift(callable $f): callable {
  return function () use ($f) {
    // This fold op first checks all the arguments are valid.
    if(array_reduce(func_get_args(), function (bool $status, Maybe $m) {
        return $m->isNothing() ? false : $status;
      },
      true
    )) {
      $args = array_map(fn (Maybe $m) => $m->getOrElse(null), func_get_args());

      // If the criteria above is met, the callable is wrapped in a Just type.
      return Maybe::just(call_user_func($f, ...$args));
    }

    // Otherwise there's nothing to wrap and a Nothing is safely returned.
    return Maybe::nothing();
  };
}

// function add(int $a, int $b): int {
//   return $a + $b;
// }

// $add2 = lift('add');
// $res = $add2(Maybe::just(1), Maybe::just(5)->getOrElse('nothing'));


/**
 * The `Either` type.
 *
 * A more generic Maybe type, it has left and right values instead of value
 * and nothing. Only left or right can be present at any time.
 *
 * In contrast with the `Maybe` type, the `Either` type provides a richer context
 * for errors within the left value, while the "right" value holds the valid
 * data because obviously it is "the right value".
 *
 * If wrapping/lifting with Either, be mindful that the wrapper can only
 * specify one error at a time, out of a multitude of errors that might
 * bubble up from the wrapped entities.
 */
abstract class Either {}
